FROM cypress/browsers:latest

WORKDIR /lighthouse
COPY package.json yarn.lock ./

RUN yarn install
RUN npm install -g @lhci/cli

COPY ./ ./
RUN yarn build

ENTRYPOINT ["lhci", "autorun"]
